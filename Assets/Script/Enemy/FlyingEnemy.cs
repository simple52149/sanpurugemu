using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : MonoBehaviour
{
    [Header("加算スコア")] public int myScore;
    [Header("画面外でも行動するか")] public bool NonVisible;
    [Header("移動速度")] public float speed;
    [Header("移動距離")] public float movingDistance;
    [Header("上昇する速度")] public float floatSpeed;
    [Header("加速度")] public float acceleration;
    [Header("ジャンプする高さ")] public float jumpHeight;
    [Header("ジャンプ制限時間")] public float jumpLimitTime;
    [Header("重力")] public float gravity;
    [Header("接触判定")] public EnemyCollisionCheck checkCollision;
    [Header("敵オブジェクト")] public GameObject EnemyObj;
    [Header("踏まれた時SE")] public AudioClip TreadOnSE;

    private SpriteRenderer sr = null;
    private Rigidbody2D rb = null;
    private Animator anim = null;
    private ObjectCollision oc = null;
    private BoxCollider2D col = null;

    private int xVector = -1;//デフォルトは左向きです 負の値…左に進む　正の値…右に進む

    private float firstPosY = 0.0f;
    private float firstPosX = 0.0f;

    private bool rightTleftF = false;
    [HideInInspector] public bool isDown = false;
    private bool isFloat = false;
    private bool rising = false;

    private string EnemyTag = "Enemy";


    // Start is called before the first frame update
    void Start()
    {
        if (EnemyObj != null)
        {
            sr = GetComponent<SpriteRenderer>();
            rb = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            oc = GetComponent<ObjectCollision>();
            col = GetComponent<BoxCollider2D>();

            firstPosX = transform.position.x;//X座標の初期位置
            firstPosY = transform.position.y;//Y座標の初期位置

            StartCoroutine(Move());
        }
        else
        {
            Debug.Log("インスペクターに敵オブジェクトが入ってないよ！！");
            Destroy(this);
        }

    }
    IEnumerator Move()
    {
        while (true)
        {
            if (!oc.playerStepOn)
            {
                //生存時の処理
                if (sr.isVisible || NonVisible)
                {
                    if (EnemyObj.tag == EnemyTag)
                    {
                        MainMove();
                        float ySpeed = getYspeed();
                        rb.velocity = new Vector2(xVector * speed, ySpeed);

                    }
                }
                else
                {
                    rb.Sleep();
                }
                yield return null;
            }
            else if (!isDown)
            {
                EnemyDie();
            }
            else
            {
                transform.Rotate(new Vector3(0, 0, 1));
                yield return null;
            }
        }
    }


    /// <summary>
    /// 動いている時
    /// </summary>
    public void MainMove()
    {
        float distanceX = firstPosX - movingDistance;//負の値なので 左の方向に進みます

        //移動する場所についたら
        if (rightTleftF)
        {
            if (transform.position.x <= distanceX)
            {
                xVector = 1;
                transform.localScale = new Vector3(-1, 1, 1);//右向きに
                rightTleftF = !rightTleftF;
            }
        }
        else
        {
            if (transform.position.x >= firstPosX)
            {
                xVector = -1;
                transform.localScale = (new Vector3(1, 1, 1));//左向き
                rightTleftF = !rightTleftF;
            }
        }

    }

    public float getYspeed()
    {
        float ySpeed = 0;

        float distanceY = firstPosY + jumpHeight;//移動する距離

        //上昇初期設定
        if (!isFloat)
        {
            ySpeed = -floatSpeed; //初速
            isFloat = true;//フラグをON
        }
        //上昇中
        else
        {
            if (!rising)
            {
                //上昇中
                if (transform.position.y < distanceY)
                {
                    ySpeed += acceleration;
                }
                //最高地点まで到達したらフラグをオンに
                else
                {
                    rising = true;
                }
  
            }
            //下降中
            else
            {
                //下降中
                if (transform.position.y > firstPosY)
                {
                    ySpeed -= acceleration;
                }
                //初期位置に到達したら
                else
                {
                    isFloat = false;
                    rising = false;
                }
            }
        }
        return ySpeed;
    }

    /// <summary>
    /// プレイヤーに踏まれた時
    /// </summary>
    public void EnemyDie()
    {
        GManager.Instance.PlaySE(TreadOnSE);
        anim.Play("FlyEnemy_Down");
        rb.velocity = new Vector2(0,Mathf.Abs(floatSpeed) * -1);
        isDown = true;
        col.enabled = false;
        if (GManager.Instance != null)
        {
            GManager.Instance.score += myScore;
        }
        Destroy(gameObject, 3f);
    }

}