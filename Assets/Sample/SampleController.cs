﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SampleController : MonoBehaviour {

    /// <summary>ボタン</summary>
    [SerializeField] private Button button;
    /// <summary>文章</summary>
    [SerializeField] private InputField messageText;

    public delegate void CallBackEvent(string text, string text2);
    public CallBackEvent callBackAction;

    private void Awake()
    {
        string sampleText = "controller側で用意したテキスト";
        
        // ボタンが押された時の処理を登録する
        button.onClick.AddListener(()=>
        {
            string input = messageText.text.ToString();
            // イベントが登録されていれば処理する。
            if (callBackAction != null) callBackAction(sampleText, input);
        });

    }

}
