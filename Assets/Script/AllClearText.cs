using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllClearText : MonoBehaviour
{
    private Text allClearText = null;

    // Start is called before the first frame update
    void Start()
    {
        allClearText = GetComponent<Text>();
        StartCoroutine(TextEffect());
    }
    IEnumerator TextEffect()
    {
        allClearText.text = "";
        yield return new WaitForSeconds(4.5f);
        allClearText.text = "プレイしていただきありがとうございます";
        yield return new WaitForSeconds(5.4f);
        allClearText.text = "これといったすごい演出はありませんが";
        yield return new WaitForSeconds(5.4f);
        allClearText.text = "最後に平沢進　現象の花の秘密をバックに閉幕となります";
        yield return new WaitForSeconds(5.4f);
        allClearText.text = "どうか心ゆくまでお楽しみください";
        yield return new WaitForSeconds(5.4f);
        allClearText.text = "Thank you for playing!";
        yield return new WaitForSeconds(5.1f);
        allClearText.text = "スペースキーを押すとタイトルに戻ります";
    }
}
