using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeImage : MonoBehaviour
{
    [Header("最初からフェードインが完了しているか")] public bool firstFadeInComp;

    private Image img = null;
    private int FrameCount = 0;
    private float timer = 0.0f;
    private bool fadeIn = false;
    private bool fadeOut = false;
    private bool compFafeIn = false;
    private bool compFafeOut = false;
    /// <summary>
    /// フェードインを開始する
    /// </summary>
    public void StartFadeIn()
    {
        if(fadeIn || fadeOut)//フェード中に別のフェードが開始しないようにする
        {
            return;
        }
        //フェード始めるための初期設定
        fadeIn = true;
        compFafeIn = false;
        timer = 0.0f;
        img.color = new Color(1, 1, 1, 1);
        img.fillAmount = 1;
        img.raycastTarget = true;//フェード中にボタンを押せないようにする）
    }
    /// <summary>
    /// フェードインが完了したかどうか
    /// </summary>
    /// <returns></returns>
    public bool IsFadeInComplete()
    {
        return compFafeIn;
    }
    /// <summary>
    /// フェードアウトを開始する
    /// </summary>
    public void StartFadeOut()
    {
        if (fadeIn || fadeOut)//フェード中に別のフェードが開始しないようにする
        {
            return;
        }
        //フェード始めるための初期設定
        fadeOut = true;
        compFafeOut = false;
        timer = 0.0f;
        img.color = new Color(1, 1, 1, 0);
        img.fillAmount = 0;
        img.raycastTarget = true;//フェード中にボタンを押せないようにす
    }
    /// <summary>
    /// フェードアウトを完了したかどうか
    /// </summary>
    /// <returns></returns>
    public bool IsFadeOutComplete()
    {
        return compFafeOut;
    }
    // Start is called before the first frame update
    void Start()
    {
        img = GetComponent<Image>();
        if (firstFadeInComp)
        {
            FadeInComplete();
        }
        else
        {
            StartFadeIn();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (FrameCount > 2)
        {
            if (fadeIn)
            {
                FadeInUpdate();
            }
            else if (fadeOut)
            {
                FadeOutUpdate();
            }
        }
        ++FrameCount;
    }
    //フェードイン中
    private void FadeInUpdate()
    {
        //フェード中
        if (timer < 1f)
        {
            img.color = new Color(1, 1, 1, 1 - timer);
            img.fillAmount = 1 - timer;
        }
        //フェード完了
        else
        {
            FadeInComplete();
        }
        timer += Time.deltaTime;
    }
    //フェードアウト中
    private void FadeOutUpdate()
    {
        if(timer < 1f)
        {
            img.color = new Color(1, 1, 1, timer);
            img.fillAmount = timer; //時間経過で１に近づくように
        }
        else
        {
            FadeOutComplete();
        }
        timer += Time.deltaTime;
    }
    //フェードイン完了
    private void FadeInComplete()
    {
        img.color = new Color(1, 1, 1, 0);
        img.fillAmount = 0;
        img.raycastTarget = false;
        timer = 0.0f;
        fadeIn = false;
        compFafeIn = true;
    }
    //フェードアウト完了
    private void FadeOutComplete()
    {
        img.color = new Color(1, 1, 1, 1);
        img.fillAmount = 1;
        img.raycastTarget = false;
        timer = 0.0f;
        fadeOut = false;
        compFafeOut = true;
    }
}


