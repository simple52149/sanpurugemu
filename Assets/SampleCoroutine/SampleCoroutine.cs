using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SampleCoroutine : MonoBehaviour
{

    public Text stateText;

    public List<int> coroutineNumber = new List<int>();
    private int number = 1;

    private void Awake()
    {
        stateText.text = "コルーチン停止中";

    }


    void Update()
    {
        //スペースキーを押してコルーチンスタート
        if (Input.GetKeyDown("space"))
        {
            coroutineNumber.Add(number);
            number++;
            StartCoroutine(CoroutineTest());
            stateText.text = "コルーチン実行中";
        }
        else if (Input.GetKeyDown("1"))
        {
            StopAllCoroutines();
            stateText.text = "全コルーチン停止中";
        }
    }

    IEnumerator CoroutineTest()
    {
        int count = 0;

        while (true)
        {
            Debug.Log("私は" + coroutineNumber[number - 2] + "番目に呼ばれたコルーチンです" + count);
            count++;
            //一秒待ってから
            yield return new WaitForSeconds(1.0f);
            //次のフレームに飛ばす
            yield return null;
        }
    }
}
