﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SampleManager : MonoBehaviour
{

    /* [説明]
     * 
     *  Manager - Controller - 判定要素
     *                       - 判定要素
     *          - Controller - 判定要素
     *                       - 判定要素
     *                       
     *  のような縦ピラミッド型の連携を作る。
     *  　
     *  Manager ⇒ ScoreItem、HeartItem など　
     *  Controller ⇒ PlayerTriggerCheck
     *  
     *  
     *  一度処理を登録しておけば、
     *  判定要素 ⇒ Controller ⇒ Manager と処理が戻っていきManager側のメソッド実行が可能になる。
     *  
     *  この方法だと、トリガーが発火したタイミングだけ処理が動くので、
     *  ScoreItemなどで毎フレームチェックしているUpdateの中身をゼロにできる。
     *
     */
         
    [SerializeField] private GameObject controllerObj;

    private void Awake()
    {
        controllerObj.GetComponent<SampleController>().callBackAction += (text, text2) =>
        {
            PushButtonAction(text, text2);
        };

    }
    
    /// <summary>
    /// ボタンを押した時の処理
    /// </summary>
    private void PushButtonAction(string str, string str2)
    {
        Debug.Log("ボタンが押された");
        Debug.Log(str + "を引数で受け取れる！");
        Debug.Log("入力した文字：" + str2);
    }
}
