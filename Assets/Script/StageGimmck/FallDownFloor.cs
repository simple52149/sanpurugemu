using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDownFloor : MonoBehaviour
{
    [Header("スプライトがあるオブジェクト")] public GameObject spriteObj;
    [Header("振動幅")] public float vibrationWidth = 0.05f;
    [Header("振動速度")] public float vibrationSpeed = 30.0f;
    [Header("落ちるまでの時間")] public float fallTime = 1.0f;
    [Header("落ちていく速度")] public float fallSpeed = 10.0f;
    [Header("落ちてから戻っていく時間")] public float returnTime = 5.0f;
    [Header("振動アニメーション")] public AnimationCurve curve;

    private bool isOn;
    private bool isFall;
    private bool isReturn;
    private Vector3 spriteDefualtPos;
    private Vector3 floorDefualtPos;
    private Vector2 FallVelocity;
    private BoxCollider2D col;
    private Rigidbody2D rb;
    private ObjectCollision oc;
    private SpriteRenderer sr;
    private float Timer = 0.0f;
    private float fallingTimer = 0.0f;
    private float returnTimer = 0.0f;
    private float blinkTimer = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        oc = GetComponent<ObjectCollision>();
        if (spriteObj != null && oc != null && col != null && rb != null)
        {
            spriteDefualtPos = spriteObj.transform.position;
            FallVelocity = new Vector2(0, -fallSpeed);
            floorDefualtPos = gameObject.transform.position;
            sr = spriteObj.GetComponent<SpriteRenderer>();
            if (sr == null)
            {
                Debug.Log("FallDownFloor インスペクターに設定し忘れがあります");
                Destroy(this);
            }
        }
        else
        {
            Debug.Log("FallDownFloor インスペクターに設定し忘れがあります");
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //プレイヤーが1回でも乗ったらフラグをオンにする
        if (oc.playerStepOn)
        {
            isOn = true;
            oc.playerStepOn = false;
        }
        if (isOn && !isFall)
        {
            //float x = curve.Evaluate(Timer * vibrationSpeed) * vibrationWidth;
            float x = vibrationWidth * Mathf.Sin(vibrationSpeed * Timer);
            spriteObj.transform.position = spriteDefualtPos + new Vector3(x, 0, 0);

            //一定時間揺れたら
            if(Timer > fallTime)
            {
                isFall = true;
                spriteObj.transform.position = spriteDefualtPos;
            }

            Timer += Time.deltaTime;
        }

        //一定時間経つと明滅して戻る
        if (isReturn)
        {
            //明滅ついているときに戻る
            if(blinkTimer > 0.2f)
            {
                sr.enabled = true;
                blinkTimer = 0.0f;
            }
            //明滅消えているとき
            else if(blinkTimer > 0.1f)
            {
                sr.enabled = false;
            }
            //明滅ついているとき
            else
            {
                sr.enabled = true;
            }
            
            //一定時間経過したら元の状態に
            if(returnTimer > 1.0f)
            {
                isReturn = false;
                blinkTimer = 0.0f;
                returnTimer = 0.0f;
                sr.enabled = true;
            }
            else
            {
                blinkTimer += Time.deltaTime;
                returnTimer += Time.deltaTime;
            }
        }
    }

    private void FixedUpdate()
    {
        //落下中
        if (isFall)
        {
            rb.velocity = FallVelocity;

            //一定時間経つと元の位置に戻る
            if(fallingTimer > returnTime)
            {
                isReturn = true;
                transform.position = floorDefualtPos;
                spriteObj.transform.position = spriteDefualtPos;
                rb.velocity = Vector2.zero;
                isFall = false;
                Timer = 0.0f;
                fallingTimer = 0.0f;
            }
            else
            {
                fallingTimer += Time.deltaTime;
                isOn = false;
            }
        }
    }
}
