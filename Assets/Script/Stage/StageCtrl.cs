using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageCtrl : MonoBehaviour
{
    [Header("プレイヤーゲームオブジェクト")] public GameObject PlayerObj; //プレイヤーを動かす為の設定
    [Header("コンテニュー位置")] public GameObject[] continuePoint; //目印を設定する
    [Header("ゲームオーバー")] public GameObject gameOverObj;
    [Header("ゲームオーバーSE")] public AudioClip gameOverSE;
    [Header("フェード")] public FadeImage fade;
    [Header("ステージクリアSE")] public AudioClip stageClearSE;
    [Header("ステージクリア")] public GameObject stageClearObj;
    [Header("ステージクリア判定")] public PlayerTriggerCheck stageClearTrigger;
    [Header("ボスエフェクト")] public GameObject BossEffectObj;
    [Header("ボス戦判定")] public PlayerTriggerCheck BossCheckTrigger;

    private player p;
    private int nextStageNum;

    private bool startFade = false;
    private bool doGameOver = false;
    private bool retryGame = false;
    public bool doSceneChange = false;
    private bool doClear = false;
    private bool doBossEffect;　

    // Start is called before the first frame update
    void Start()
    {
        if(PlayerObj != null
           && continuePoint != null 
           && continuePoint.Length > 0 
           && gameOverObj != null 
           && fade != null)
        {
            gameOverObj.SetActive(false);
            stageClearObj.SetActive(false);
            BossEffectObj.SetActive(false);
            PlayerObj.transform.position = continuePoint[0].transform.position;

            p = PlayerObj.GetComponent<player>();//プレイヤーのスクリプトを取得
            if(p == null)
            {
                Debug.Log("プレイヤーではないものがアタッチされているよ");
            }
        }
        else
        {
            Debug.Log("設定が足りてないよ");
        }
    }

    // Update is called once per frame
    void Update()
    {
        //ゲームオーバー時の処理
        if (GManager.Instance.isGameOver && !doGameOver)
        {
            gameOverObj.SetActive(true);
            GManager.Instance.PlaySE(gameOverSE);
            doGameOver = true;
        }
        //プレイヤーがやられたとき
        else if (p != null 
                 && p.IsContinueWaiting() 
                 && !doGameOver)
        {
            if (p != null && p.IsContinueWaiting())
            {
                if (continuePoint.Length > GManager.Instance.continueNum)
                {
                    p.transform.position = continuePoint[GManager.Instance.continueNum].transform.position;
                    p.ContinuePlayer();
                }
                else
                {
                    Debug.Log("どこにコンテポイント入れとんねん");
                }
            }
        }
        //ステージクリア時
        else if (stageClearTrigger != null
                && stageClearTrigger.isOn
                && !doGameOver
                && !doClear
                && !doBossEffect)
        {
            StageClear();
            doClear = true;
        }
        else if(BossCheckTrigger != null
                && BossCheckTrigger.isOn
                && !doGameOver
                && !doClear
                && !doBossEffect)
        {
            BossEffect();
            doBossEffect = true;
        }

        //ステージを切り替える
        if (fade != null && startFade  && !doSceneChange)
        {
            if (fade.IsFadeOutComplete())
            {
                //ゲームリトライ
                if (retryGame)
                {
                    GManager.Instance.RetryGame();
                }
                //次のステージへ
                else
                {
                    GManager.Instance.stageNum = nextStageNum;
                }
                if (GManager.Instance.StageNumber.Length > nextStageNum)
                {
                    GManager.Instance.TimeLimit = GManager.Instance.FirstTimeLimit;
                    GManager.Instance.isStageClear = false;
                    GManager.Instance.continueNum = 0;
                    SceneManager.LoadScene("stage" + nextStageNum);
                    doSceneChange = true;
                }
                //作ったステージ数を超えたらelseに行きます
                else
                {
                    SceneManager.LoadScene("Ending");
                }
            }
        }
    }



    /// <summary>
    /// 最初から始める
    /// </summary>
    public void Retry()
    {
        ChangeScene(1);
        retryGame = true;
    }


    /// <summary>
    /// シーン切り替え
    /// </summary>
    /// <param name="num">ステージ番号</param>
    public void ChangeScene(int num)
    {
        if (fade != null)
        {
            nextStageNum = num;
            fade.StartFadeOut();
            startFade = true;
        }
    }

    /// <summary>
    /// ステージクリア
    /// </summary>
   public void StageClear()
    {
        GManager.Instance.isStageClear = true;
        stageClearObj.SetActive(true);
        GManager.Instance.PlaySE(stageClearSE);
    }

    public void BossEffect()
    {
        BossEffectObj.SetActive(true);
    }
}
