using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEnemy : MonoBehaviour
{

    [Header("加算スコア")] public int myScore;
    [Header("画面外でも行動するか")] public bool NonVisible;
    [Header("移動速度")] public float speed;
    [Header("重力")] public float gravity;
    [Header("接触判定")] public EnemyCollisionCheck checkCollision;
    [Header("敵オブジェクト")] public GameObject EnemyObj;
    [Header("判定のスクリプト")] public PlayerTriggerCheck Check;
    [Header("クリアエフェクト")] public GameObject StageClearObj;
    [Header("演出")] public GameObject BossEffectObj;
    [Header("踏む回数")] public int BossTreadOnCount;

    private Rigidbody2D rb = null;
    private Animator anim = null;
    private ObjectCollision oc = null;
    private BoxCollider2D col = null;
    private SpriteRenderer sr = null;
    private bool rightTleftF = false;
    private bool isTread = false;
    private bool isDown = false;
    private float blinkTime = 0.0f;
    private float timer = 0.0f;
    private string EnemyTag = "Enemy";

    // Start is called before the first frame update
    void Start()
    {
            rb = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            oc = GetComponent<ObjectCollision>();
            col = GetComponent<BoxCollider2D>();
            sr = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (EnemyObj != null)
        {
            EneMove();
        }
        else
        {
            Debug.Log("オブジェクト入れてよおおおおおおおおお");
            Destroy(this);
        }
    }
    private void EneMove()
    {
        if (EnemyObj.tag == EnemyTag)
        {
            if (!oc.playerStepOn) //プレイヤーにふまれたかどうか
            {

                if (!oc.playerStepOn && isTread)
                {
                    gravity = -0.1f;
                    col.enabled = false;
                    if (blinkTime > 0.2f)
                    {
                        sr.enabled = true;
                        blinkTime = 0.0f;
                    }
                    else if (blinkTime > 0.1f)
                    {
                        sr.enabled = false;
                    }
                    else
                    {
                        sr.enabled = true;
                    }

                    //3秒経過したら終わり
                    if (timer > 3.0f)
                    {
                        blinkTime = 0.0f;
                        timer = 0.0f;
                        sr.enabled = true;
                        col.enabled = true;
                        isTread = false;
                        gravity = 3.0f;
                    }
                    else
                    {
                        blinkTime += Time.deltaTime;
                        timer += Time.deltaTime;
                    }
                }
                if (sr.isVisible || NonVisible)
                {

                    if (checkCollision.isOn)
                    {
                        rightTleftF = !rightTleftF;
                    }
                    int xVector = -1; //左向き
                    if (rightTleftF)
                    {
                        xVector = 1;
                        transform.localScale = new Vector3(-5, 5, 1); //右向きになる

                    }
                    else
                    {
                        transform.localScale = new Vector3(5, 5, 1);
                    }
                    rb.velocity = new Vector2(xVector * speed, -gravity);
                }
                else
                {
                    rb.Sleep();
                }
            }
            else
            {
                isTread = true;
                //踏む回数が1回なら通らない
                if (oc.playerStepOn && BossTreadOnCount > 1)
                {
                    --BossTreadOnCount;
                    oc.playerStepOn = false;
                }
                else
                {
                    if (!isDown)
                    {
                        anim.Play("Enemy_down");
                        rb.velocity = new Vector2(0, -gravity);
                        isDown = true;
                        col.enabled = false;

                        if (GManager.Instance != null)
                        {
                            GManager.Instance.score += myScore;
                        }
                        Destroy(gameObject, 3f);
                    }
                    else
                    {
                        transform.Rotate(new Vector3(0, 0, 5));
                        StageClearObj.SetActive(true);
                    }
                }
            }
        }
    }
}
