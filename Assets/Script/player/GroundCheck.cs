using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    [Header("エフェクトがついた床を判定するか")] public bool checkPlatformGround;

    private string grounfTag = "Ground";
    private string platformTag = "GroundPlatform";
    private string moveFloorTag = "MoveFloor";
    private string fallFloorTag = "FallFloor";
    private bool isGround = false;
    private bool isGroundEnter, isGroundStay, isGroundExit;

    //物理演算の判定毎に呼ぶ必要がある
    public bool IsGround()
    {
        if (isGroundEnter || isGroundStay)
        {
            isGround = true;
        }
        else if (isGroundExit)
        {
            isGround = false;
        }

        isGroundEnter = false;
        isGroundStay = false;
        isGroundExit = false;

        return isGround;
    }

    private void OnTriggerEnter2D(Collider2D collision) 
    {
        if(collision.tag == grounfTag)
        {
            isGroundEnter = true;
        }
        else if (checkPlatformGround && collision.tag == platformTag || collision.tag == moveFloorTag || collision.tag == fallFloorTag)
        {
            isGroundEnter = true;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == grounfTag) 
        {
            isGroundStay = true;
        }
        else if(checkPlatformGround && collision.tag == platformTag || collision.tag == moveFloorTag || collision.tag == fallFloorTag)
        {
            isGroundStay = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == grounfTag) 
        {
            isGroundExit = true;
        }
        else if (checkPlatformGround && collision.tag == platformTag || collision.tag == moveFloorTag || collision.tag == fallFloorTag)
        {
            isGroundExit = true;
        }
    }
}
