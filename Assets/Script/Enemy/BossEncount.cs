using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossEncount : MonoBehaviour
{

    [Header("ステージコントローラー")] public StageCtrl ctrl;
    [Header("キャンバスグループ")]public CanvasGroup cg;
    private float blinkTime = 0.0f;
    private float timer = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        if(cg == null && ctrl == null)
        {
            Debug.Log("設定して!");
            Destroy(this);
        }
        else
        {
            cg.alpha = 0.0f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        {
            //点滅　ついてるときの戻る
            if (blinkTime > 0.2f)
            {
                cg.alpha = 1.0f; //戻る
                blinkTime = 0.0f;
            }
            //点滅　消えているとき
            else if (blinkTime > 0.1f)
            {
                cg.alpha = 0.0f; //オフ
            }
            //点滅　ついている時 
            else
            {
                cg.alpha = 1.0f; //オン
            }

            //1秒経ったら点滅終わり
            if (timer > 1.0f)
            {
                blinkTime = 0.0f;
                cg.alpha = 0.0f;
                
            }
            else
            {
                blinkTime += Time.deltaTime;
                timer += Time.deltaTime;
            }
        }
    }
}

