using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    #region//インスペクターで設定
    #region //プレイヤー動作関連
    [Header("移動速度")]public float speed; //速度
    [Header("ジャンプ速度")] public float jumpSpeed; //ジャンプ速度
    [Header("ジャンプする高さ")] public float jumpheight; //ジャンプする高さ
    [Header("ジャンプ制限時間")] public float jumpLimitTime; //ジャンプ制限時間
    [Header("踏みつけ判定の高さの割合")]public float StepOnRate;
    [Header("重力")] public float gravity; //重力
    [Header("ダッシュの速さ表現")] public AnimationCurve dashCurve;
    [Header("ジャンプの速さ表現")] public AnimationCurve jumpCurve;
    #endregion

    #region//接触判定関連
    [Header("接地判定")] public GroundCheck ground; //接地判定のスクリプト
    [Header("頭をぶつけた判定")] public GroundCheck head; //頭をぶつけた判定
    #endregion

    #region//SE関連
    [Header("ジャンプSE")] public AudioClip jumpSE;
    [Header("棘に当たった時のSE")] public AudioClip NeedleHitSE;
    [Header("落下死の時のSE")] public AudioClip DeadAreaSE;
    [Header("敵にぶつかった時のSE")] public AudioClip EnemyCollideSE;
    #endregion
    #endregion

    #region//プライベート変数
    #region//インスタンス関連
    private Animator anim = null;
    private Rigidbody2D rb = null;
    private CapsuleCollider2D capool = null;
    private SpriteRenderer sr = null;
    private MoveObject moveObj = null;
    #endregion

    #region//int型
    private int FirstTimeLimit;
    #endregion

    #region//bool型
    private bool isGround = false;
    private bool isHead = false;
    private bool isJump = false;
    private bool isRun = false;
    private bool isDown = false;
    private bool isOtherJump = false;
    private bool isContinue = false;
    private bool nonDownAnim = false;
    private bool isClearMotion = false;
    private bool SEflag = false;
    #endregion

    #region//float型
    private float continueTime = 0.0f;
    private float blinkTime = 0.0f;
    private float jumpPos = 0.0f;
    private float otherJumpHeight = 0.0f;
    private float jumpTime = 0.0f;
    private float dashTime = 0.0f;
    private float beforeKey = 0.0f;
    #endregion

    #region//string型
    private string EnemyTag = "Enemy";
    private string deadAreaTag = "DeadArea";
    private string hitAreaTag = "HitArea";
    private string moveFloorTag = "MoveFloor";
    private string FallFloorTag = "FallFloor";
    #endregion
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        //コンポーネントのインスタンスを捕まえている
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        capool = GetComponent<CapsuleCollider2D>();
        sr = GetComponent<SpriteRenderer>();

        FirstTimeLimit = GManager.Instance.TimeLimit;
    }

    private void Update()
    {

        if (isContinue) 
        { 
            //点滅　ついてるときの戻る
            if(blinkTime > 0.2f)
            {
                sr.enabled = true;　//戻る
                blinkTime = 0.0f;
            }
            //点滅　消えているとき
            else if(blinkTime > 0.1f)
            {
                sr.enabled = false; //オフ
            }
            //点滅　ついている時 
            else
            {
                sr.enabled = true; //オン
            }

            //1秒経ったら点滅終わり
            if(continueTime > 1.0f)
            {
                isContinue = false;
                blinkTime = 0.0f;
                continueTime = 0.0f;
                sr.enabled = true;
            }
            else
            {
                blinkTime += Time.deltaTime;
                continueTime += Time.deltaTime;
            }
        }
    }

    /// <summary>
    /// メインメソッドですプレイヤーを動かす関連はここからお願いします。
    /// </summary>
    void FixedUpdate()
    {
        //プレイヤーが動く条件
        if (!isDown 
            && !GManager.Instance.isGameOver
            && !GManager.Instance.isStageClear
            && GManager.Instance.TimeLimit != 0)
        {
            //接地判定を取得
            isGround = ground.IsGround();
            isHead = head.IsGround();

            //各種座標軸を求める
            float ySpeed = GetYspeed();
            float xSpeed = GetXspeed();

            //アニメーションを適用
            SetAnimation();

            //移動速度を設定
            Vector2 addVelocity = Vector2.zero;
            if(moveObj != null)
            {
                addVelocity = moveObj.GetVelocity();
            }
            rb.velocity = new Vector2(xSpeed, ySpeed) + addVelocity;
        }
        else
        {
            if(!isClearMotion && GManager.Instance.isStageClear)
            {
                anim.Play("player_jumpdown");
                isClearMotion = true;
            }
            if(GManager.Instance.TimeLimit == 0)
            {
                ReceiveDamage(true);
            }
            rb.velocity = new Vector2(0, -gravity);//ダウン中は落下しかできないように
        }
    }
    /// <summary>
    /// Y成分で必要な計算をし、速度を返すメソッド
    /// </summary>
    /// <returns>Y軸の速さ</returns>
    public float GetYspeed()
    {
        float verticalkey = Input.GetAxis("Vertical");
        float ySpeed = -gravity;

        if (isOtherJump) //ジャンプ中の処理
        {
            //現在が飛べる高さより下か
            bool canHeight = jumpPos + otherJumpHeight > transform.position.y;
            //ジャンプ時間が長くなりすぎていないか
            bool canTime = jumpLimitTime > jumpTime;

            if (canHeight && canTime && !isHead)
            {
                ySpeed = jumpSpeed;
                jumpTime += Time.deltaTime;
            }
            else
            {
                isOtherJump = false;
                jumpTime = 0.0f;
            }
        }

        if (isGround) //地面についているとき
        {
            if (verticalkey > 0) //上方向のキーが押されている
            {
                if (!isJump)
                {
                    GManager.Instance.PlaySE(jumpSE);
                }
                jumpPos = transform.position.y;//ジャンプした位置を記録
                ySpeed = jumpSpeed; //Y軸を設定したものに
                isJump = true;
                jumpTime = 0.0f;
            }
            else
            {
                isJump = false;//連打対策
            }
        }
        else if (isJump) //ジャンプ中の処理
        {
            //上方向キーが押されているか
            bool pushUpkey = verticalkey > 0;
            //現在が飛べる高さより下か
            bool canHeight = jumpPos + jumpheight > transform.position.y;
            //ジャンプ時間が長くなりすぎていないか
            bool canTime = jumpLimitTime > jumpTime;

            if (pushUpkey && canHeight && canTime && !isHead)
            {
                ySpeed = jumpSpeed;
                jumpTime += Time.deltaTime;
            }
            else
            {
                isJump = false;
                jumpTime = 0.0f;
            }
        }
        if (isJump || isOtherJump)
        {
            ySpeed *= jumpCurve.Evaluate(jumpTime);
        }
        return ySpeed;
    }
    /// <summary>
    /// X成分で必要な計算をし、速度を返す
    /// </summary>
    /// <returns>X軸の速さ</returns>
    public float GetXspeed()
    {
        float xSpeed = 0.0f;
        float horizontalkey = Input.GetAxis("Horizontal");

        if (horizontalkey > 0) //右を押しているとき
        {
            transform.localScale = new Vector3(1, 1, 1);
            isRun = true;
            dashTime += Time.deltaTime;
            xSpeed = speed;

        }
        else if (horizontalkey < 0) //左を押しているとき
        {
            transform.localScale = new Vector3(-1, 1, 1);
            isRun = true;
            dashTime += Time.deltaTime;
            xSpeed = -speed;
        }
        else //入力無し
        {
            isRun = false;
            dashTime = 0.0f;
            xSpeed = 0.0f;
        }
        //前回の入力からダッシュの反転を判断して速度を変える(前回との入力と今回の入力が違うとき
        if (horizontalkey > 0 && beforeKey < 0)
        {
            dashTime = 0.0f;
        }
        else if (horizontalkey < 0 && beforeKey > 0)
        {
            dashTime = 0.0f;
        }
        beforeKey = horizontalkey; //加速をリセット

        //アニメーションカーブの値を速度に適用
        xSpeed *= dashCurve.Evaluate(dashTime);

        return xSpeed;
    }
    /// <summary>
    /// アニメーションを設定する
    /// </summary>
    public void SetAnimation()
    {
        anim.SetBool("run", isRun);
        anim.SetBool("jump", isJump || isOtherJump);
        anim.SetBool("ground", isGround);
    }
    #region//接触判定
    public void OnCollisionEnter2D(Collision2D collision)
    {

        bool enemy = collision.collider.tag == EnemyTag;
        bool moveFloor = collision.collider.tag == moveFloorTag;
        bool fallFloor = collision.collider.tag == FallFloorTag;

        if (enemy || moveFloor || fallFloor)
        {
            //踏みつけ判定になる高さ
            float StepOnHeight = (capool.size.y * (StepOnRate / 100f));

            //踏みつけ判定のワールド座標
            float judgePos = transform.position.y - (capool.size.y / 2f) + StepOnHeight;

            foreach (ContactPoint2D p in collision.contacts)
            {
                if (p.point.y < judgePos) //衝突した位置が判定内ならば
                {
                    if (enemy || fallFloor)
                    {
                        //もう一度跳ねる
                        ObjectCollision o = collision.gameObject.GetComponent<ObjectCollision>();
                        if (o != null)
                        {
                            if (enemy)
                            {
                                otherJumpHeight = o.BoundHeight; //踏んづけたものから跳ねる高さを取得
                                o.playerStepOn = true;//踏んづけたものに対して踏んづけたことを通知する
                                jumpPos = transform.position.y;//ジャンプした位置を記録
                                isOtherJump = true;
                                isJump = false;
                                jumpTime = 0.0f;
                            }
                            else if (fallFloor)
                            {
                                o.playerStepOn = true;
                            }
                        }
                        else
                        {
                            Debug.Log("ObjectCollisionがないよ");
                        }
                    }
                    else if (moveFloor)
                    {
                        moveObj = collision.gameObject.GetComponent<MoveObject>();
                    }
                }
                else
                {
                    if (enemy)
                    {
                        //ダウンする
                        ReceiveDamage(true);
                        GManager.Instance.PlaySE(EnemyCollideSE);
                        break;
                    }
                }
            }
            
        }
        else if (collision.collider.tag == moveFloorTag)
        {
            //踏みつけ判定になる高さ
            float StepOnHeight = (capool.size.y * (StepOnRate / 100f));

            //踏みつけ判定のワールド座標
            float judgePos = transform.position.y - (capool.size.y / 2f) + StepOnHeight;

            foreach (ContactPoint2D p in collision.contacts)
            {
                if (p.point.y < judgePos) //衝突した位置が判定内ならば
                {
                    //動く床に乗っている
                    moveObj = collision.gameObject.GetComponent<MoveObject>();
                }
            }

        }
    }
    #endregion


    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.collider.tag == moveFloorTag)
        {
            moveObj = null;
        }
    }

    /// <summary>
    /// ダメージ受ける系統のオブジェクトはここからお願いします
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == deadAreaTag)
        {
            if (!SEflag)
            {
                GManager.Instance.PlaySE(DeadAreaSE);
                SEflag = true;
            }
            ReceiveDamage(false); //落下判定
        }
        else if(collision.tag == hitAreaTag)
        {
            if (!SEflag)
            {
                GManager.Instance.PlaySE(NeedleHitSE);
                SEflag = true;
            }
            ReceiveDamage(true); //接触判定
        }

    }
    /// <summary>
    /// コンティニュー待機状態か
    /// </summary>
    /// <returns></returns>
    public bool IsContinueWaiting()
    {
        if (GManager.Instance.isGameOver) //ゲームオーバーのときはコンテしない
        {
            return false;
        }
        else 
        {
            return IsDownAnimEnd() || nonDownAnim; //ダウンアニメーションをしない場合もコンテできるように
        }
    }
    private bool IsDownAnimEnd()
    {
        if(isDown && anim != null) //やられているか、アニメーターは取得できているか
        {
            AnimatorStateInfo currentState = anim.GetCurrentAnimatorStateInfo(0);
            if (currentState.IsName("player_down")) //再生しているアニメがdownかどうか
            {
                if(currentState.normalizedTime >= 1) //再生が100％以上かどうか
                {
                    return true;
                }
            }
        }
        return false; //アニメーションが終わってなかったら
    }

    /// <summary>
    /// コンテニュー時各種フラグ操作はここからお願いします
    /// </summary>
    public void ContinuePlayer()
    {
        //各種フラグのリセット
        isDown = false;
        anim.Play("player_stand anim");
        isJump = false;
        isOtherJump = false;
        isRun = false;
        isContinue = true;
        nonDownAnim = false;
        SEflag = false;

        GManager.Instance.TimeLimit = FirstTimeLimit;

        rb.velocity = new Vector2(0, 0);
    }

    /// <summary>
    /// ダメージを受けたい時はこのメソッドからお願いします
    /// </summary>
    /// <param name="downAnim">ダウンアニメを再生したいなら引数でtrueをお願いします</param>
    private void ReceiveDamage(bool downAnim)
    {
        if (isDown || GManager.Instance.isStageClear)
        {
            return;
        }
        else 
        {
            if (downAnim) //ダウンアニメーションをするか
            {
                anim.Play("player_down");
            }
            else 
            {
                nonDownAnim = true;
            }
        }
        //既にダウン状態ではない時
        anim.Play("player_down");
        isDown = true;
        GManager.Instance.SubHeartNum();
    }
}
