using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageClearEffect : MonoBehaviour
{
    public StageCtrl ctrl;

    private bool comp = false;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = Vector3.zero;
        StartCoroutine(MainSystem());
    }

   IEnumerator MainSystem()
    {
        if (!comp)
        {
            transform.position = Vector3.one;
            comp = true;
            yield return new WaitForSeconds(3.0f);
        }
        else
        {
            ctrl.ChangeScene(GManager.Instance.stageNum + 1);
        }
    }
}
