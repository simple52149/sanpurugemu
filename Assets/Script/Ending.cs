using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Ending : MonoBehaviour
{
    [Header("フェード処理スクリプト")]public FadeImage fade;
    [Header("全クリメッセージ")]public AllClearText allClearText;
    private bool titleBackFlag = false;


    private void Update()
    {
        if (fade.IsFadeInComplete())
        {
            if (!titleBackFlag && Input.GetKeyDown("space"))
            {
                fade.StartFadeOut();
                titleBackFlag = true;
            }

            if (titleBackFlag && fade.IsFadeOutComplete())
            {
                GManager.Instance.isStageClear = false;
                GManager.Instance.HeartNum = GManager.Instance.defualtHeartNum;
                GManager.Instance.score = 0;
                GManager.Instance.stageNum = 1;
                GManager.Instance.continueNum = 0;
                GManager.Instance.TimeLimit = GManager.Instance.FirstTimeLimit;
                SceneManager.LoadScene("Title");
            }

        }
    }
}