using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeActiveUGUI : MonoBehaviour
{
    [Header("フェードスピード")] public float fadespeed = 1.0f;
    [Header("上昇量")] public float moveDis = 10.0f;
    [Header("上昇時間")] public float moveTime = 1.0f;
    #region //メッセージの追加するときはここの配列に入れてください 0番目は使わないのでダミーオブジェクトを入れて下さい
    [Header("プレイヤー判定")] public PlayerTriggerCheck[] trigger;
    [Header("スプライトがあるオブジェクト")] public GameObject[] SpriteObj;
    [Header("キャンバスグループ")] public CanvasGroup  [] cg;
    #endregion 
    [Header("メッセージ表示時SE")] public AudioClip MessegeActiveSE;

    private List<Vector3> defaultPos = new List<Vector3>();
    private List<int> number = new List<int>();
    private int ActiveNumber;
    private int InactiveNumber;
    private float timer = 0.0f;
    private bool SEflag = false;
    
    // Start is called before the first frame update
    void Start()
    {
 
        for(int n = 0; n < cg.Length; ++n)
        {

            if(cg[n] == null && trigger[n] == null && SpriteObj[n] == null)
            {
                Debug.Log("インスペクターの設定が足りてないよ！！");
                Destroy(this);
            }
            else
            {
                cg[n].alpha = 0.0f;
                defaultPos.Add(cg[n].transform.position);
                number.Add(n);
                cg[n].transform.position = defaultPos[n] - Vector3.up * moveDis;
                SpriteObj[n].SetActive(true);
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        //プレイヤーがどのフラグを踏んでいるか探索
        //デフォルトは配列のLengthが返されます
        for (int n = 0; n < cg.Length && !trigger[n].isOn; ++n)
        {
            //フラグがオンの時の数字を記憶　この時は配列の数字の１個手前を記憶してます
            ActiveNumber = n;
   
        }
        //ここで配列の位置を合致
        ++ActiveNumber;

        //配列の要素数を超えたとき、減算してエラーにならないようにします
        if(ActiveNumber == cg.Length)
        {
            --ActiveNumber;
        }

        if (trigger[ActiveNumber].isOn)
        {
            //踏んでいたフラグの位置を別の変数に記憶フラグがOFFの時の処理はこいつがやります
            InactiveNumber = ActiveNumber;

            SpriteObj[ActiveNumber].SetActive(false);
            if (!SEflag)
            {
                SEflag = true;
                GManager.Instance.PlaySE(MessegeActiveSE);
            }
            //上昇してフェードインする
            if (cg[ActiveNumber].transform.position.y < defaultPos[ActiveNumber].y || cg[ActiveNumber].alpha < 1.0f)
            {
                cg[ActiveNumber].alpha = timer / moveTime;
                cg[ActiveNumber].transform.position += Vector3.up * (moveDis * moveTime) * fadespeed * Time.deltaTime;
                timer += fadespeed * Time.deltaTime;
            }
            //フェードイン完了したら
            else
            {
                cg[ActiveNumber].alpha = 1.0f;
                cg[ActiveNumber].transform.position = defaultPos[ActiveNumber];
            }
        }
        else
        {
            //下降してフェードアウトする
            if (cg[InactiveNumber].transform.position.y > defaultPos[InactiveNumber].y - moveDis || cg[InactiveNumber].alpha > 0.0f)
            {
                cg[InactiveNumber].alpha = timer / moveTime;
                cg[InactiveNumber].transform.position -= Vector3.up * (moveDis * moveTime) * fadespeed * Time.deltaTime;
                timer -= fadespeed * Time.deltaTime;
            }
            //フェードアウト完了
            else
            {
                timer = 0.0f;
                cg[InactiveNumber].alpha = 0.0f;
                cg[InactiveNumber].transform.position = defaultPos[InactiveNumber] - Vector3.up * moveDis;
                SEflag = false;
                SpriteObj[InactiveNumber].SetActive(true);
            }
        }
    }
}
