using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreItem : MonoBehaviour
{
    [Header("加算するスコア")] public int myScore;
    [Header("プレイヤーの判定")] public PlayerTriggerCheck playerCheck;
    [Header("アイテム取得SE")] public AudioClip GetItemSE;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerCheck.isOn) 
        { 
            if(GManager.Instance != null)
            {
                GManager.Instance.PlaySE(GetItemSE);
                GManager.Instance.score += myScore;
                Destroy(this.gameObject);
            }
        }
    }
}
