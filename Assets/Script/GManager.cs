using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GManager : MonoBehaviour
{
    public static GManager Instance = null;

    [Header("スコア")] public int score;
    [Header("現在のステージ")] public int stageNum;
    [Header("現在の復帰位置")] public int continueNum;
    [Header("現在の残機")] public int HeartNum;
    [Header("デフォルトの残機")] public int defualtHeartNum;
    [Header("制限時間")] public int TimeLimit;
    [HideInInspector] public int FirstTimeLimit;
    [HideInInspector] public bool isGameOver;
    [HideInInspector] public bool isStageClear = false;
    [Header("ステージ作ったらステージの番号を追加してください")]public int[] StageNumber;


    private AudioSource AudioSource = null;
    private float Timer = 0.0f;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        AudioSource = GetComponent<AudioSource>();
        FirstTimeLimit = TimeLimit;

    }

    // Update is called once per frame
    void Update()
    {
        Timer += Time.deltaTime;
        //制限時間が0になるまでの間
        if (TimeLimit != 0)
        {
            //デルタイムが１になったら制限時間から引く
            if (Timer > 1f)
            {
                TimeLimit -= (int)Timer;
                Timer = 0.0f;
            }
        }
    }
    /// <summary>
    /// 残機を増やす　上限値:99
    /// </summary>
    public void AddHeartNum()
    {
        if(HeartNum < 99)
        {
            ++HeartNum;
        }
    }
    /// <summary>
    /// 残機を減らす
    /// </summary>
    public void SubHeartNum()
    {
        if(HeartNum > 0)
        {
            --HeartNum;
        }
        else
        {
            isGameOver = true;
        }
    }

    public void RetryGame()
    {
        isGameOver = false;
        HeartNum = defualtHeartNum;
        score = 0;
        stageNum = 1;
        continueNum = 0;
    }

    public void PlaySE(AudioClip clip)
    {
        if(AudioSource != null)
        {
            AudioSource.PlayOneShot(clip);
        }
        else
        {
            Debug.Log("何も入ってないよ〜ん");
        }
    }
    public void PlayBGM(AudioClip clip)
    {
        if (AudioSource != null)
        {
            AudioSource.PlayOneShot(clip);
        }
        else
        {
            Debug.Log("何も入ってないよ〜ん");
        }
    }
}
