using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("加算スコア")] public int myScore;
    [Header("画面外でも行動するか")] public bool NonVisible;
    [Header("移動速度")] public float speed;
    [Header("重力")] public float gravity;
    [Header("接触判定")] public EnemyCollisionCheck checkCollision;
    [Header("敵オブジェクト")] public GameObject EnemyObj;
    [Header("踏まれた時SE")] public AudioClip TreadOnSE;

    private SpriteRenderer sr = null;
    private Rigidbody2D rb = null;
    private Animator anim = null;
    private ObjectCollision oc = null;
    private BoxCollider2D col = null;
    private bool rightTleftF = false;
    [HideInInspector]public bool isDown = false;
    private string EnemyTag = "Enemy";


    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        oc = GetComponent<ObjectCollision>();
        col = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (EnemyObj != null)
        {
            if (EnemyObj.tag == EnemyTag)
            {
                EnemyMove();
            }

        }
        else
        {
            Debug.Log("オブジェクト入れてよおおおおおおおおお");
            Destroy(this);
        }
    }
    private void EnemyMove()
    {
        if (!oc.playerStepOn) //プレイヤーにふまれたかどうか
        {
                if (sr.isVisible || NonVisible)
                {

                if (checkCollision.isOn)
                {
                    rightTleftF = !rightTleftF;//向きが逆になるfalse(左向き),true(右向き)
                }
                    int xVector = -1; //左向き
                    if (rightTleftF)
                    {
                        xVector = 1;
                        transform.localScale = new Vector3(-1, 1, 1); //右向きになる

                    }
                    else
                    {
                        transform.localScale = new Vector3(1, 1, 1);
                    }
                    rb.velocity = new Vector2(xVector * speed, -gravity);
                }
                else
                {
                    rb.Sleep();
                }
        }
        else
        {
            if (!isDown)
            {
                GManager.Instance.PlaySE(TreadOnSE);
                anim.Play("Enemy_down");
                rb.velocity = new Vector2(0, -gravity);
                isDown = true;
                col.enabled = false;
                if (GManager.Instance != null)
                {
                    GManager.Instance.score += myScore;
                }
                Destroy(gameObject, 3f);
            }
            else
            {
                transform.Rotate(new Vector3(0, 0, 5));
            }
        }
    }


}
