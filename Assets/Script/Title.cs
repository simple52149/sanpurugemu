using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    [Header("フェード")] public FadeImage fade;

    private bool firstpush = false;
    private bool goNextScene = false;
    //スタートボタンを押したら呼ばれる
    public void PressStart()
    {
        if (!firstpush) //falseじゃないと通らない
        {
            fade.StartFadeOut();
            firstpush = true; //trueになるので一度しか通らない
        }
    }
    //フェードアウトの完了を監視
    private void Update()
    {
        if(!goNextScene && fade.IsFadeOutComplete())
        {
            SceneManager.LoadScene("stage1");
            goNextScene = true;
        }
    }
}
