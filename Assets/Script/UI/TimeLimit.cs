using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeLimit : MonoBehaviour
{
    private Text TimeText = null;
    private int oldTimeLimit;

    // Start is called before the first frame update
    void Start()
    {
        TimeText = GetComponent<Text>();
        if (GManager.Instance != null)
        {
            TimeText.text = "Time" + GManager.Instance.TimeLimit;
        }
        else
        {
            Debug.Log("ゲームマネージャーがないよ！");
            Destroy(this);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (oldTimeLimit != GManager.Instance.TimeLimit)
        {
            TimeText.text = "Time"  +  GManager.Instance.TimeLimit;
            oldTimeLimit = GManager.Instance.TimeLimit;
        }
    }
}