using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundEnemy : MonoBehaviour
{
    [Header("加算スコア")] public int myScore;
    [Header("画面外でも行動するか")] public bool NonVisible;
    [Header("移動速度")] public float speed;
    [Header("移動距離")] public float movingDistance;
    [Header("ジャンプ速度")] public float jumpSpeed;
    [Header("加速度")] public float acceleration;
    [Header("ジャンプする高さ")] public float jumpHeight;
    [Header("ジャンプ制限時間")] public float jumpLimitTime;
    [Header("重力")] public float gravity;
    [Header("接触判定")] public EnemyCollisionCheck checkCollision;
    [Header("敵オブジェクト")] public GameObject EnemyObj;
    [Header("踏まれた時SE")] public AudioClip TreadOnSE;
    [Header("踏まれた時SE")] public AudioClip jumpSE;

    private SpriteRenderer sr = null;
    private Rigidbody2D rb = null;
    private Animator anim = null;
    private ObjectCollision oc = null;
    private BoxCollider2D col = null;

    private int xVector = -1;//デフォルトは左向きです 負の値…左に進む　正の値…右に進む

    public float jumpPos = 0.0f;
    private float firstPosY = 0.0f;
    public float jumpTime = 0.0f;
    private float firstPosX = 0.0f;
    private float distance = 0.0f;
    private float nowPosX = 0.0f;

    private bool rightTleftF = false;
    [HideInInspector] public bool isDown = false;
    public bool isGround = false;
    public bool isJump = false;

    private string EnemyTag = "Enemy";


    // Start is called before the first frame update
    void Start()
    {
        if (EnemyObj != null)
        {
            sr = GetComponent<SpriteRenderer>();
            rb = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            oc = GetComponent<ObjectCollision>();
            col = GetComponent<BoxCollider2D>();

            firstPosX = transform.position.x;//X座標の初期位置
            distance = firstPosX - movingDistance;//負の値なので 左の方向に進みます
            firstPosY = transform.position.y;//Y座標の初期位置
            StartCoroutine(Move());
        }
        else
        {
            Debug.Log("インスペクターに敵オブジェクトが入ってないよ！！");
            Destroy(this);
        }

    }
    IEnumerator Move()
    {
        if (!oc.playerStepOn)
        {
            //生存時の処理
            if (sr.isVisible || NonVisible)
            {
                if (EnemyObj.tag == EnemyTag)
                {
                    MainMove();
                    float ySpeed = getYspeed();
                    rb.velocity = new Vector2(xVector * speed, ySpeed);

                }
            }
            else
            {
                rb.Sleep();
            }
            StartCoroutine(Move());
            yield return !oc.playerStepOn;
        }
        else if (!isDown)
        {
            EnemyDie();
        }
        else
        {
            transform.Rotate(new Vector3(0, 0, 1));
            yield return null;
        }
        
    }


    /// <summary>
    /// 動いている時
    /// </summary>
    public void MainMove()
    {
        nowPosX = transform.position.x;//現在のX軸の位置を保存
        //移動する場所についたら
        if (rightTleftF)
        {
            if (nowPosX <= distance)
            {
                xVector = 1;
                transform.localScale = new Vector3(-1, 1, 1);//右向きに
                rightTleftF = !rightTleftF;
            }
        }
        else
        {
            if(nowPosX >= firstPosX)
            {
                xVector = -1;
                transform.localScale = (new Vector3(1, 1, 1));//左向き
                rightTleftF = !rightTleftF;
            }
        }

    }

    public float getYspeed()
    {
            float ySpeed = 0;

            //ジャンプしてない時
            if (!isJump)
            {

                jumpPos = transform.position.y;//現在の位置を記録
                ySpeed = -jumpSpeed; //初速
                isJump = true;//フラグをON
                jumpTime = 0.0f; //時間をリセット

            }
            else
            {

                //飛べる高さより下にいるか
                bool canHeight = jumpPos + jumpHeight > transform.position.y;

                //ジャンプ制限時間内か
                bool canJumpTime = jumpLimitTime > jumpTime;

                if (canHeight && canJumpTime)
                {
                    ySpeed += acceleration;
                    jumpTime += Time.deltaTime;
                }
                else if (firstPosY < transform.position.y)
                {
                    ySpeed -= acceleration;
                }
                else
                {
                    isJump = false;
                    jumpTime = 0.0f;
                }
            }
        return ySpeed;
    }

    /// <summary>
    /// プレイヤーに踏まれた時
    /// </summary>
    public void EnemyDie()
    {
        GManager.Instance.PlaySE(TreadOnSE);
        anim.Play("Enemy_down");
        rb.velocity = new Vector2(0, -gravity);
        isDown = true;
        col.enabled = false;
        if (GManager.Instance != null)
        {
            GManager.Instance.score += myScore;
        }
        Destroy(gameObject, 3f);
    }

}

