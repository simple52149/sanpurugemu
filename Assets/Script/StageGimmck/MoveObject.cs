using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    [Header("移動経路")] public GameObject [] movePoint;
    [Header("速さ")] public float speed = 1.0f;
    [Header("画面外でも動くか")] public bool NonVisible;

    private Rigidbody2D rb = null;
    private SpriteRenderer sr = null;
    private int nowPonint = 0;
    private bool returnPoint = false;
    private Vector2 oldPos = Vector2.zero;
    private Vector2 myVelocity = Vector2.zero;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        if(nowPonint != null && movePoint.Length > 0 && rb != null)
        {
            rb.position = movePoint[0].transform.position;
            oldPos = rb.position; //初期位置を記録
        }
    }

    //外から速度を読めるように
    public Vector2 GetVelocity()
    {
        return myVelocity;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (sr.isVisible || NonVisible)
        {
            if (nowPonint != null && movePoint.Length > 1 && rb != null)
            {
                //通常進行
                if (!returnPoint)
                {
                    int nextPoint = nowPonint + 1;

                    //目標のポイントとの差が僅かになるまで移動
                    if (Vector2.Distance(transform.position, movePoint[nextPoint].transform.position) > 0.1f)
                    {
                        //現在地から次のポイントのベクトルを作成
                        Vector2 toVector = Vector2.MoveTowards(transform.position, movePoint[nextPoint].transform.position, speed * Time.deltaTime);

                        //次のポイントへ移動
                        rb.MovePosition(toVector);
                    }
                    //次のポイントを１つ進める
                    else
                    {
                        rb.MovePosition(movePoint[nextPoint].transform.position);
                        ++nowPonint;

                        //現在地が配列の最後だったら
                        if (nowPonint + 1 >= movePoint.Length)
                        {
                            returnPoint = true;
                        }
                    }
                }
                else
                {
                    int nextPoint = nowPonint - 1;

                    //目標のポイントとの差が僅かになるまで移動
                    if (Vector2.Distance(transform.position, movePoint[nextPoint].transform.position) > 0.1f)
                    {
                        //現在地から次のポイントのベクトルを作成
                        Vector2 toVector = Vector2.MoveTowards(transform.position, movePoint[nextPoint].transform.position, speed * Time.deltaTime);

                        //次のポイントへ移動
                        rb.MovePosition(toVector);
                    }
                    //次のポイントを１つ進める
                    else
                    {
                        rb.MovePosition(movePoint[nextPoint].transform.position);
                        --nowPonint;

                        //現在地が配列の最初だったら
                        if (nowPonint <= 0)
                        {
                            returnPoint = false;
                        }
                    }
                }
            }
            myVelocity = (rb.position - oldPos) / Time.deltaTime; //現在の位置 - 前の位置 = 進んだ距離
            oldPos = rb.position; //前の位置を記録
        }
        else
        {
            rb.Sleep();
        }
    }
}
