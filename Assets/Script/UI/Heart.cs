using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Heart : MonoBehaviour
{

    private Text heartText = null;

    private int oldHeartNum;
    private float blinkTime = 0.0f;
    private float Timer = 0.0f;

    private bool NonFirst = false;
    private bool blinkSwitch = false; 

    // Start is called before the first frame update
    void Start()
    {
        heartText = GetComponent<Text>();
        if (GManager.Instance != null)
        {
            heartText.text = "×" + GManager.Instance.HeartNum;
        }
        else
        {
            Debug.Log("ゲームマネージャーがないよ！");
            Destroy(this);

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (oldHeartNum != GManager.Instance.HeartNum)
        {
            oldHeartNum = GManager.Instance.HeartNum;
            blinkSwitch = true;
        }

        if (blinkSwitch)
        {
            if (!NonFirst)
            {
                //ステージ開始時のみ通る

                heartText.text = "×" + GManager.Instance.HeartNum;
                oldHeartNum = GManager.Instance.HeartNum;
                NonFirst = true;
                blinkSwitch = false;
            }

            //点滅　ついてるときの戻る
            if (blinkTime > 0.2f)
            {
                heartText.text = "×" + GManager.Instance.HeartNum;
                blinkTime = 0.0f;
            }
            //点滅　消えているとき
            else if (blinkTime > 0.1f)
            {
                heartText.text = "";//オフ
            }
            //点滅　ついている時 
            else
            {
                heartText.text = "×" + GManager.Instance.HeartNum;//オン
            }

            //3秒経ったら点滅終わり
            if (Timer > 3.0f)
            {
                blinkTime = 0.0f;
                Timer = 0.0f;
                heartText.text = "×" + GManager.Instance.HeartNum;
                blinkSwitch = false;
            }
            else
            {
                blinkTime += Time.deltaTime;
                Timer += Time.deltaTime;
            }
        }
    }
}
