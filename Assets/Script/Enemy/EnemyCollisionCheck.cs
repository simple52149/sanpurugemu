using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisionCheck : MonoBehaviour
{
  /// <summary>
    /// 判定内に壁がある
    /// </summary>
    [HideInInspector] public bool isOn = false;

    private string groundTag = "Ground";
    private string enemyTag = "Enemy";
    private string bossEnemyTag = "BossEnemy";
    private string hitAreaTag = "HitArea";

    #region
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == groundTag 
            || collision.tag == enemyTag 
            || collision.tag == bossEnemyTag
            || collision.tag == hitAreaTag) 
        {
            isOn = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == groundTag
            || collision.tag == enemyTag
            || collision.tag == bossEnemyTag
            || collision.tag == hitAreaTag)
        {
            isOn = false;
        }
    }
}  
    #endregion